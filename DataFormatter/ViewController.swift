//
//  ViewController.swift
//  dsfghk
//
//  Created by Antonio Elefante on 25/10/2019.
//  Copyright © 2019 Antonio Elefante. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        var text = ""
        if let path = Bundle.main.path(forResource:"Data" , ofType: "txt") {
            do {
                text = try String(contentsOfFile:path, encoding: String.Encoding.utf8)
                
                } catch { print("Failed to read text from bundle file \(self)") }
        } else { print("Failed to load file from bundle \(self)") }
        
        let data:[String] = text
        .split(separator: " ", omittingEmptySubsequences:true)
        .map(String.init)
        
        var matrix = Array<Array<String>>()
        var i = 0
        var j = 0
        var array = Array<String>()
        for str in data {
            array.insert(str, at: i)
            i = i + 1
            if(str.contains("\n")) {
                var newStr = str
                newStr.removeLast()
                array.removeLast()
                array.insert(newStr, at: i-1)
                matrix.insert(array, at: j)
                i = 0
                j = j+1
                array = Array<String>()
            }
        }
        
        print(matrix)
        
    }
    


}

